import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }
    
    getProducts() {
        let products = localStorage.getItem('products');
        return JSON.parse(products);
    }
    
    pushProduct(obj) {
        let products = this.getProducts();
        if (!products) {
            products = [];
        }
        products.push(obj);
        localStorage.setItem('products', JSON.stringify(products));
    }
}
