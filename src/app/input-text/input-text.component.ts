import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR  } from '@angular/forms';

@Component({
  selector: 'input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputTextComponent),
      multi: true
    }
  ]
})
export class InputTextComponent implements ControlValueAccessor, OnInit {
  
  /* All of this hell only for append validator classes 
     to parent elements
  */
  
  private _value = '';

  // setter for watch changes and update model
  set value(value: string) {
    this._value = value;
    this.writeValue(value);
  }
  // as a model for <input>
  get value(): string { return this._value; }
  
  // input parameters
  @Input() label:string = '';
  @Input() append:string = '';
  @Input() id:string = '';
  constructor() { }

  ngOnInit() {
  }
  
  click(e) {
  }
  
  disabled:boolean = false;
  onChange = (value: string) => {};
  onTouched = () => {};
  
  // ControlValueAccessor methods
  
  writeValue(value: string): void {
    this._value = value;
    this.onChange(this._value)
  }
  
  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
  
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
