import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators   } from '@angular/forms';
import { StorageService } from '../storage.service';

@Component({
  selector: 'product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
  dataModel: FormGroup;
  
  // typically this data served on DB and has an IDs, but for simplify..
  discounts: number[] = [0,5,10,15,20,25,30,35,40,45,50];
  powerTypes: string[] = [
    'Аккумуляторный',
    'От сети',
  ];
  types: string[] = [
    'Обычный',
    'Вертикальный',
    'Ручной',
    'Робот',
    '2 в 1',
  ];
  powerControllers: string[] = [
    'Есть',
    'Нет'
  ];
  cleaningTypes: string[] = [
    'Сухая',
    'Влажная',
    'Сухая и влажная',
  ];
  autoCloses: string[] = [
    'Есть',
    'Нет',
  ];
  collectorTypes: string[] = [
    "Без мешка",
    "С мешком",
    "Аквафильтр",
  ];
  
  /*
    getters for simplify (used in template)
  */
  get isBattery() {
    return this.dataModel.get('powerType').value == 'Аккумуляторный'
  }
  get empFormArray(): FormArray{
    return this.dataModel.get('colors') as FormArray;
  }
  // not in model, because it is a computed property
  get sum(): number {
    let price:number = this.dataModel.get('price').value;
    let discount:number = this.dataModel.get('discount').value;
    return price - price*discount/100 ;
  }
  
  constructor( private storageService: StorageService, private fb: FormBuilder) { };
   
  // init
  ngOnInit() {
    this.initForm();
    /* something more.. */
  }
  
  // fill model
  initForm(){
    this.dataModel = this.fb.group({
      name: ['',
      [
        Validators.required,
        Validators.minLength(4),
        Validators.pattern(/[а-яёА-ЯЁA-Za-z0-9]/)
      ]],
      code: ['',
      [
        Validators.required,
        Validators.minLength(4),
        Validators.pattern(/[а-яёА-ЯЁA-Za-z0-9]/)
      ]],
      price: [null,
      [
        Validators.required,
        Validators.min(1),
        Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
      ]],
      discount: [0],
      type: ['', [
        Validators.required,
      ]],
      cleaningType: ['', [
        Validators.required,
      ]],
      powerController: ['', [
        Validators.required,
      ]],
      powerType: ['От сети', [
        Validators.required,
      ]],
      powerCable: this.fb.group({
        cableLength: ['', [
          Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)  
        ]],
        autoClose: ['']
      }),
      powerBattery: this.fb.group({
        capacity: ['', [
          Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)  
        ]],
        workingTime: ['', [
          Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)  
        ]]
      }),
      collectorType: ['', [
        Validators.required,
      ]],
      collectorCapacity: [null, [
        Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
      ]],
      noise: [null, [
        Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
      ]],
      length: [null, [
        Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
      ]],
      width: [null, [
        Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
      ]],
      weight: [null, [
        Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
      ]],
      height: [null, [
        Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
      ]],
      colors: [[], [
         Validators.required,
         Validators.minLength(1)
      ]],
      image: '',
    });
    
    // init watcher
    this.onDataChanges();
  }

  onDataChanges(): void {
    this.dataModel.get('type').valueChanges.forEach(val => {
      console.dir(val)
    });
  }
  
  // for test purposes
  click(a) {
    console.log(a)
  }
  
  submit() {
    if (this.dataModel.invalid) {
      let controls = this.dataModel.controls;
      console.dir(this.dataModel.value);
      Object.keys(controls).forEach((controlName) => { 
        controls[controlName].markAsTouched()
      });
      return;
    }
    
    this.storageService.pushProduct(this.dataModel.value);
  }
  

}
