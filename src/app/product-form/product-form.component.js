"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var OrderFormComponent = (function () {
    function OrderFormComponent(fb) {
        this.fb = fb;
        // typically this data served on DB and has an IDs, but for simplify..
        this.discounts = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50];
        this.powerTypes = [
            'Аккумуляторный',
            'От сети',
        ];
        this.types = [
            'Обычный',
            'Вертикальный',
            'Ручной',
            'Робот',
            '2 в 1',
        ];
        this.powerControllers = [
            'Есть',
            'Нет'
        ];
        this.cleaningTypes = [
            'Сухая',
            'Влажная',
            'Сухая и влажная',
        ];
        this.autoCloses = [
            'Есть',
            'Нет',
        ];
        this.collectorTypes = [
            "Без мешка",
            "С мешком",
            "Аквафильтр",
        ];
        this.src = '';
    }
    Object.defineProperty(OrderFormComponent.prototype, "isBattery", {
        /*
          getters for simplify (used in template)
        */
        get: function () {
            return this.dataModel.get('powerType').value == 'Аккумуляторный';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OrderFormComponent.prototype, "empFormArray", {
        get: function () {
            return this.dataModel.get('colors');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OrderFormComponent.prototype, "sum", {
        // not in model, because it is a computed property
        get: function () {
            var price = this.dataModel.get('price').value;
            var discount = this.dataModel.get('discount').value;
            return price - price * discount / 100;
        },
        enumerable: true,
        configurable: true
    });
    ;
    // init
    OrderFormComponent.prototype.ngOnInit = function () {
        this.initForm();
        /* something more.. */
    };
    // fill model
    OrderFormComponent.prototype.initForm = function () {
        this.dataModel = this.fb.group({
            name: ['',
                [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(4),
                    forms_1.Validators.pattern(/[а-яёА-ЯЁA-Za-z0-9]/)
                ]],
            code: ['',
                [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(4),
                    forms_1.Validators.pattern(/[а-яёА-ЯЁA-Za-z0-9]/)
                ]],
            price: [null,
                [
                    forms_1.Validators.required,
                    forms_1.Validators.min(1),
                    forms_1.Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
                ]],
            discount: [0],
            type: ['', [
                    forms_1.Validators.required,
                ]],
            cleaningType: ['', [
                    forms_1.Validators.required,
                ]],
            powerController: ['', [
                    forms_1.Validators.required,
                ]],
            powerType: ['От сети', [
                    forms_1.Validators.required,
                ]],
            powerCable: this.fb.group({
                cableLength: [''],
                autoClose: ['']
            }),
            powerBattery: this.fb.group({
                capacity: ['', [
                        forms_1.Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
                    ]],
                workingTime: ['']
            }),
            collectorType: ['', [
                    forms_1.Validators.required,
                ]],
            collectorCapacity: [null, [
                    forms_1.Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
                ]],
            noise: [null, [
                    forms_1.Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
                ]],
            length: [null, [
                    forms_1.Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
                ]],
            width: [null, [
                    forms_1.Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
                ]],
            weight: [null, [
                    forms_1.Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
                ]],
            height: [null, [
                    forms_1.Validators.pattern(/^(\d){1,}([.][\d]{1,})?$/u)
                ]],
            colors: [['red', 'blue']],
            image: ''
        });
        // init watcher
        this.onDataChanges();
    };
    OrderFormComponent.prototype.onDataChanges = function () {
        this.dataModel.get('type').valueChanges.forEach(function (val) {
            console.dir(val);
        });
    };
    // for test purposes
    OrderFormComponent.prototype.click = function (a) {
        console.log(a);
    };
    OrderFormComponent = __decorate([
        core_1.Component({
            selector: 'app-order-form',
            templateUrl: './order-form.component.html',
            styleUrls: ['./order-form.component.css']
        })
    ], OrderFormComponent);
    return OrderFormComponent;
}());
exports.OrderFormComponent = OrderFormComponent;
