import { Component, OnInit } from '@angular/core';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-orders',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products = [];
  selectedIndex = 0;
  
  constructor(private storageService: StorageService) { }

  ngOnInit() {
    this.products = this.storageService.getProducts() || [];
  }

  get selectedProduct() {
    return (this.products[this.selectedIndex]) ? this.products[this.selectedIndex] : [];
  }

  selectProduct(i) {
    this.selectedIndex = i;
    console.dir(this.selectedProduct);
  }

}
