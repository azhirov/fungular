import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckColorComponent } from './check-color.component';

describe('CheckColorComponent', () => {
  let component: CheckColorComponent;
  let fixture: ComponentFixture<CheckColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckColorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
