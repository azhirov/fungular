import { Component, EventEmitter, Input, Output, OnInit  } from '@angular/core';

@Component({
  selector: 'check-color',
  templateUrl: './check-color.component.html',
  styleUrls: ['./check-color.component.css']
})

export class CheckColorComponent implements OnInit {
  @Input() color: string = '';
  @Input() checked: boolean = false;
  @Output() onClicked = new EventEmitter<boolean>();
  
  get class() {
    let arr = [this.color];
    if (this.checked) {
      arr.push('checked');
    }
    return arr;
  }
  constructor() { }

  ngOnInit() {
  }
  
  toggle() {
    this.onClicked.emit(!this.checked);
  }
  
}
