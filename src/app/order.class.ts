export class Order {
  id: number;
  price: number = 0;
  name: string;
  discount: number;
}