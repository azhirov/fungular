import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductFormComponent }      from './product-form/product-form.component';
import { ProductsComponent }      from './products/products.component';


 const routes: Routes = [
    { path: '', component: ProductFormComponent },
    { path: 'products', component: ProductsComponent }
  ];

@NgModule({
  exports: [
    RouterModule,
  ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule { 
 
  
}
