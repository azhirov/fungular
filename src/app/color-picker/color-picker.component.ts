import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR  } from '@angular/forms';

@Component({
  selector: 'color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ColorPickerComponent),
      multi: true
    }
  ]
})
export class ColorPickerComponent implements ControlValueAccessor, OnInit {
  
  // available colors
  colors:string[] = [
    'red',
    'orange',
    'yellow',
    'green',
    'blue',
    'black',
    'white'
  ];
  
  // selected colors
  checked:string[] = [];
  
  disabled:boolean = false;
  onChange = (value: string[]) => {};
  onTouched = () => {};
  
  constructor() { }
  
  ngOnInit() {
  }
  
  toggle(color) {
    let arr = this.checked.slice(0);
    if (arr.indexOf(color) != -1) {
      arr.splice(arr.indexOf(color),1);
    } else {
      arr.push(color);
    }
    console.log(arr);
    this.writeValue(arr);
  }
  
  // ControlValueAccessor methods
  
  writeValue(value: string[]): void {
    this.checked = value;
    this.onChange(this.checked)
  }
  
  registerOnChange(fn: (value: string[]) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
  
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

}
