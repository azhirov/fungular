import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppNavbarComponent } from './app-navbar/app-navbar.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { CheckColorComponent } from './check-color/check-color.component';
import { ColorPickerComponent } from './color-picker/color-picker.component';
import { ImgUploaderComponent } from './img-uploader/img-uploader.component';
import { InputTextComponent } from './input-text/input-text.component';
import { InputSelectComponent } from './input-select/input-select.component';

import { StorageService } from './storage.service';
import { AppRoutingModule } from './/app-routing.module';
import { ProductsComponent } from './products/products.component';


@NgModule({
  declarations: [
    AppComponent,
    AppNavbarComponent,
    ProductFormComponent,
    CheckColorComponent,
    ColorPickerComponent,
    ImgUploaderComponent,
    InputTextComponent,
    InputSelectComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule.forRoot(),
    AppRoutingModule
  ],
  providers: [StorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
