import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR  } from '@angular/forms';

@Component({
  selector: 'img-uploader',
  templateUrl: './img-uploader.component.html',
  styleUrls: ['./img-uploader.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ImgUploaderComponent),
      multi: true
    }
  ]
})
export class ImgUploaderComponent implements ControlValueAccessor, OnInit {
  
  // data-image (because localStorage)
  image:string = '';
  error:boolean = false;
  
  disabled:boolean = false;
  onChange = (value: string) => {};
  onTouched = () => {};
  
  constructor() { }

  ngOnInit() {
  }
  
  
  // check file size & set data-image
  readFile(file) {
    if (file.size / 1024 > 1024) {
      this.error = true;
      return;
    }
    
    this.error = false;
    let reader = new FileReader();
    let self = this;
    reader.onload = function (e2 : any) {
      self.writeValue(e2.target.result.toString());
    }
    reader.readAsDataURL(file);
  }
  
  // dropzone events
  fileChangeEvent(e) {
    if (e.target.files && e.target.files[0]) {
      this.readFile(e.target.files[0]);
    } else {
      this.error = true;
      this.writeValue('');
    }
  }
  
  onDrop(e, data) {
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      this.readFile(e.dataTransfer.files[0]);
    } else {
      this.error = true;
      this.writeValue('');
    }
  }
  
  allowDrop(event) {
    event.preventDefault();
  }
  
  // ControlValueAccessor methods
  
  writeValue(value: string): void {
    this.image = value;
    this.onChange(this.image)
  }
  
  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
  
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
